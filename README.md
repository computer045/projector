# Projector

## Description

This project is a GUI library for DragonRuby projects.

## Usage

### Include in your project

- Add this repository to your game's `/lib` folder.

```bash
git clone https://gitlab.com/computer045/projector.git
```

- Add the `require` in your main ruby file

```ruby
# Projector
require 'lib/projector/projector.rb'
```

### Default variables

For buttons to work, you need to declare those variables on application startup.

```ruby
args.state.inside_button ||= false
args.state.mouse_held ||= false
```

### Panel

```ruby
main_panel = Panel.new(
  {
    pos_x: 0,
    pos_y: 0,
    width: 500,
    border: 'full', # full, top, bottom, left, right or combination of sides separated by underscore "_".
    padding_top: 60,
    padding_bottom: 0,
    padding_left: 20,
    padding_right: 20,
    padding_inspect: true
  }
)

main_panel.add_element(Label.new("This is some example text"))

outputs.primitives << main_panel.render_hash
```

### Label

```ruby
outputs.primitives << Label.new(
  "This is some example text",
  {
    pos_x: 0,
    pos_y: 0,
    width: 500,
    align: 'left', # left, center or right
    border: 'full', # full, top, bottom, left, right or combination of sides separated by underscore "_".
    text_size: 1
  }
).render_hash
```

### Button

```ruby
test_button = Button.new(
  "Button",
  {
    pos_x: 100,
    pos_y: 200,
    font: Projector::Font('mono')
  }
)

def test_button.on_click(args)
  args.state.value = "example value"
end

test_button.check_hover(args)

outputs.primitives << test_button.render_hash
```

The text added to labels can be styled with markup around words of groups of words.

- bold italic: `***example***` or `___example___`
- bold:  `**example***` or `__example__`
- italic: `*example*` or `_example_`
- code: <code>\`example\`</code>

## License

MIT
