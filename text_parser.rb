# frozen_string_literal: true

module Projector
  class TextParser

    STYLE_MARKS = [
      { symbol: :bold_italic, mark: '***' },
      # { symbol: :bold_italic, mark: '___' },
      { symbol: :bold, mark: '**' },
      # { symbol: :bold, mark: '__' },
      { symbol: :italic, mark: '*' },
      # { symbol: :italic, mark: '_' },
      { symbol: :code, mark: '`' },
      { symbol: :end, mark: '~' }
    ].freeze

    # Uses the markup positions to create an array of strings that can be
    # converted to text labels.
    # Returns an array of hashes with "style" and "text" keys
    def self.parse(text)
      text_markup_pos = find_markup_positions(text)
      cursor_pos = 0
      text_markup = []
      previous_mark_size = 0
      if text_markup_pos.empty?
        text_markup.push({ style: :regular, text: text })
      else
        text_markup_pos.each do |block|
          cursor_pos += previous_mark_size
          previous_block_size = (block[:slice][0] - cursor_pos - block[:mark].size)
          previous_block = text[cursor_pos, (previous_block_size.negative? ? 0 : previous_block_size)]
          text_markup.push({ style: :regular, text: previous_block }) unless previous_block.empty?
          text_markup.push({ style: block[:style], text: text[block[:slice][0], block[:slice][1]] })
          cursor_pos += previous_block_size + block[:slice][1] + block[:mark].size
          previous_mark_size = block[:mark].size
        end
        text_markup.push({ style: :regular, text: text[cursor_pos + previous_mark_size, (text.size - cursor_pos)] })
      end
      text_markup
    end

    # Get a list of positions of each possible style markup.
    # Returns an array of hashes with the style type, the markup chars and the
    # the index of the first char and the length of the block.
    def self.find_markup_positions(text)
      text_markup_pos = []
      STYLE_MARKS.each do |style_mark|
        markup_position = find_markup_occurrence(text, style_mark[:mark])
        next if markup_position.empty?

        markup_position.each_slice(2) do |slice|
          text_markup_pos.push(
            {
              style: style_mark[:symbol],
              mark: style_mark[:mark],
              slice: [(slice[0] + style_mark[:mark].size), (slice[1] - slice[0] - style_mark[:mark].size)]
            }
          )
        end
      end
      text_markup_pos.sort { |tm_a, tm_b| tm_a[:slice] <=> tm_b[:slice] }
    end

    # Find all the occurrences of a specific markup string
    # Returns an array of Integer
    def self.find_markup_occurrence(text, mark)
      (0..text.size).find_all do |i|
        text[i, mark.size] == mark &&
          text[i, mark.size + 1] != "#{mark}#{mark[0]}" &&
          text[i - 1, mark.size + 1] != "#{mark}#{mark[0]}"
      end
    end

    # Adds missing text markup
    def self.fix_text_markup(text)
      new_text = text
      STYLE_MARKS.each do |style_mark|
        markup_position = find_markup_occurrence(text, style_mark[:mark])
        next if markup_position.empty?

        new_text = "#{text}#{style_mark[:mark]}" if markup_position.size.odd?
      end
      new_text
    end

    def self.remove_markup(text)
      STYLE_MARKS.each do |style_mark|
        text = text.tr(style_mark[:mark], '')
      end
      text
    end

  end
end
