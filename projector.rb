# frozen_string_literal: true

# Utils
require 'lib/projector/color.rb'
require 'lib/projector/font.rb'
require 'lib/projector/text_parser.rb'


# GUI
require 'lib/projector/gui/window_element.rb'
require 'lib/projector/gui/button.rb'
require 'lib/projector/gui/label.rb'
require 'lib/projector/gui/panel.rb'
require 'lib/projector/gui/rectangle.rb'
