# frozen_string_literal: true

module Projector
  class Font

    def self.style(style_name = 'regular')
      case style_name
      when 'bold'
        'fonts/NotoSans-Bold.ttf'
      when 'italic'
        'fonts/NotoSans-Italic.ttf'
      when 'bold_italic'
        'fonts/NotoSans-BoldItalic.ttf'
      when 'code'
        'fonts/NotoSansMono-Regular.ttf'
      when 'jp'
        'fonts/NotoSansJP-Regular.ttf'
      else
        'fonts/NotoSans-Regular.ttf'
      end
    end

  end
end
