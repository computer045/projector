# frozen_string_literal: true

module Projector
  class Label < WindowElement
    attr_accessor :text, :text_size

    def initialize(text, options = {})
      super(options)
      @text = text
      @options[:type] = 'label'
      @options[:align] =
        case options[:align]
        when 'right'
          2
        when 'center'
          1
        else
          0
        end
      @options[:width] = get_string_length(text) if @options[:width].nil? || (@options[:width]).zero?
      bleeding = 1280 - @options[:pos_x]
      @options[:width] = bleeding if get_string_length(text) > bleeding
      @options[:height] = get_height(get_lines)
    end

    def get_string_length(string, font = @options[:font])
      $gtk.args.gtk.calcstringbox(string, @options[:text_size], font).x
    end

    def get_styled_line_length(styled_text)
      styled_text.map { |h| get_string_length(h[:text], Projector::Font.style(h[:style])) }.sum.to_i
    end

    def get_lines
      lines = string_to_lines(@text)
      lines.is_a?(String) ? [].push(lines) : lines
    end

    def get_height(lines)
      height = 0
      lines.each do |line|
        height += get_string_height(line)
      end
      height
    end

    def string_to_lines(string)
      return string unless get_string_length(string) > @options[:width]

      list_of_strings = []
      if get_string_length(string) > @options[:width]
        string.gsub!("\r", '')
        strings_with_linebreaks = string.split("\n")
        list_of_strings = strings_with_linebreaks.map do |line|
          next if line == ''

          line.split
        end
      end
      list_to_lines(list_of_strings)
    end

    def list_to_lines(strings)
      line = ''
      lines = []
      strings.map! do |string|
        next unless string

        string << '' # Adds a blank 'word' to the end of each outer array, to trigger newline code
      end.flatten!.pop
      strings.each do |word|
        if !word || word.empty? # Handling of blank 'words' and Nil entries in arrays
          lines.push(line.dup) unless line.empty? # Adds existing accumulated words to the current line
          lines.push(' ') if line.empty? # Adds a space if no words accrued
          line.clear # Clears the accumulator
        elsif get_string_length("#{Projector::TextParser.remove_markup(line)} #{Projector::TextParser.remove_markup(word)}") <= @options[:width] # "If current word fits on the end of the current line, do"
          line << ' ' if line.length.positive? # Inserts a space into accumulator if the line isn't blank
          line << word # Adds the current word to the accumulator
        else # "If the word doesn't fit, instead do"
          lines.push(line.dup) # Adds accumulator to current line
          line.clear # Clears accumulator
          line << word # Adds current word to accumulator
        end
      end
      lines.push(line.dup) # Add accumulator to current line, as it's possible for accumulator to not have been committed
      lines # Return array of lines, explicitly to be safe.
    end

    def render_hash
      lines = get_lines
      @options[:height] = get_height(lines)
      text_hash = []
      last_line_mark_fix = ''
      lines.each_with_index do |line, idx|
        line = "#{last_line_mark_fix}#{line}" if last_line_mark_fix != ''
        fixed_line = Projector::TextParser.fix_text_markup(line)
        diff_size = fixed_line.size - line.size
        last_line_mark_fix = diff_size.zero? ? '' : fixed_line[line.size, diff_size]
        styled_text = Projector::TextParser.parse(fixed_line)
        line_length = get_styled_line_length(styled_text)
        offset = 0
        styled_text.each do |slice|
          text_hash.push(
            {
              x: @options[:pos_x] + offset + (
                if @options[:align] == 1
                  ((@options[:width] / 2) - (line_length / 2)).to_i
                elsif @options[:align] == 2
                  (@options[:width] - line_length).to_i
                else
                  0
                end
              ),
              y: @options[:pos_y] - (idx * get_string_height(line)) + @options[:height],
              text: slice[:text],
              size_enum: @options[:text_size],
              font: Projector::Font.style(slice[:style].to_s),
              alignment_enum: 0,
              r: @options[:color].r,
              g: @options[:color].g,
              b: @options[:color].b
            }.label!
          )
          offset += get_string_length(slice[:text], Projector::Font.style(slice[:style].to_s))
        end
      end
      text_hash + border_hash
    end

    def render(args)
      args.outputs.primitives << render_hash
    end

  end
end