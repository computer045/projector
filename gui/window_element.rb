# frozen_string_literal: true

module Projector
  ## Definition of class WindowElement
  # Parent class for elements that can be added to windows
  class WindowElement
    attr_accessor :options

    def initialize(options)
      @options = options
      @options[:pos_x] = options[:pos_x].nil? ? 0 : options[:pos_x].round
      @options[:pos_y] = options[:pos_y].nil? ? 0 : options[:pos_y].round
      @options[:font] = Projector::Font.style if options[:font].nil?
      @options[:width] = options[:width].nil? ? 0 : options[:width].round
      @options[:height] = options[:height].nil? ? 0 : options[:height].round
      @options[:color] = options[:color].nil? ? { r: 0, g: 0, b: 0 } : Color.hex_to_rgb(options[:color])
      @options[:bg_transparency] = options[:bg_color].nil? ? 0 : 255
      @options[:bg_color] = options[:bg_color].nil? ? { r: 0, g: 0, b: 0 } : Color.hex_to_rgb(options[:bg_color])
      @options[:text_size] = 1 if @options[:text_size].nil?
      @options[:border_size] = 1 if @options[:border_size].nil?
      @options[:border_color] = options[:border_color].nil? ? { r: 0, g: 0, b: 0 } : Color.hex_to_rgb(options[:border_color])
    end

    def get_string_length(string, font = @options[:font])
      $gtk.args.gtk.calcstringbox(string, @options[:text_size], font).x
    end

    def get_string_height(string, font = @options[:font])
      $gtk.args.gtk.calcstringbox(string, @options[:text_size], font).y
    end

    def border_hash
      borders = Rectangle.new(
        @options[:pos_x],
        @options[:pos_y],
        @options[:width],
        @options[:height],
        @options[:border_size],
        @options[:border_color]
      ).render_hash
      borders_list = []
      borders_list = @options[:border].split('_') unless @options[:border].nil?
      borders_list = [@options[:border]] if borders_list.is_a?(String)
      rendered_border = []
      borders_list.each do |border_name|
        rendered_border.concat(
          case border_name
          when 'full'
            borders
          when 'top'
            [borders[0]]
          when 'bottom'
            [borders[1]]
          when 'left'
            [borders[2]]
          when 'right'
            [borders[3]]
          else
            []
          end
        )
      end
      rendered_border
    end

    def serialize
      @options
    end

    def inspect
      serialize.to_s
    end

    def to_s
      serialize.to_s
    end

  end
end
