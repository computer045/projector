# frozen_string_literal: true

module Projector
  class Panel < WindowElement
    attr_reader :elements

    def initialize(options = {})
      super(options)
      @options[:type] = 'panel'
      @options[:padding_inspect] = false if options[:padding_inspect].nil?
      @options[:padding_top] = 0 if options[:padding_top].nil?
      @options[:padding_bottom] = 0 if options[:padding_bottom].nil?
      @options[:padding_left] = 0 if options[:padding_left].nil?
      @options[:padding_right] = 0 if options[:padding_right].nil?
      @options[:gap] = 0 if options[:gap].nil?
      @options[:orientation] = 'horizontal' if options[:orientation].nil?
      @elements = []
    end

    def add_element(element)
      @elements.push(element)
      elements_height = 0
      elements_width = 0
      tallest_element = 0
      @elements.each_with_index do |elem, idx|
        elements_height += elem.options[:height] + (idx < (@elements.size - 1) ? @options[:gap] : 0)
        elements_width += elem.options[:width] + (idx < (@elements.size - 1) ? @options[:gap] : 0)
        tallest_element = elem.options[:height] if tallest_element < elem.options[:height]
      end
      if @options[:orientation] == 'vertical'
        @options[:height] = elements_height + @options[:padding_top] + @options[:padding_bottom]
        absolute_width = @options[:width] - @options[:padding_left] - @options[:padding_right]
        @options[:width] = (element.options[:width] + @options[:padding_left] + @options[:padding_right]) if element.options[:width] > absolute_width
      elsif @options[:orientation] == 'horizontal'
        @options[:height] = tallest_element + @options[:padding_top] + @options[:padding_bottom]
        @options[:width] = elements_width + @options[:padding_left] + @options[:padding_right]
      end
    end

    def set_elements_positions
      elements_width = 0
      @elements.each_with_index do |elem, idx|
        if @options[:orientation] == 'horizontal'
          elem.options[:pos_y] = @options[:pos_y] + @options[:padding_bottom]
          elem.options[:pos_x] = @options[:pos_x] + (idx > 0 ? elements_width: 0) + @options[:padding_left] + (idx * @options[:gap])
          elements_width += elem.options[:width]
        elsif @options[:orientation] == 'vertical'
          elem.options[:pos_y] = @options[:pos_y] + @options[:height] - ((idx + 1) * elem.options[:height]) - @options[:padding_top] - (idx * @options[:gap])
          elem.options[:pos_x] = @options[:pos_x] + @options[:padding_left]
        end
      end
    end

    def render_hash
      set_elements_positions
      padding_hash =
        if @options[:padding_inspect]
          [
            {
              x: @options[:pos_x],
              y: @options[:pos_y],
              w: @options[:padding_left],
              h: @options[:height],
              r: 0, g: 255, b: 0
            }.solid!,
            {
              x: @options[:pos_x] + @options[:width] - @options[:padding_right],
              y: @options[:pos_y],
              w: @options[:padding_right],
              h: @options[:height],
              r: 0, g: 255, b: 0
            }.solid!,
            {
              x: @options[:pos_x],
              y: @options[:pos_y] + @options[:height] - @options[:padding_top],
              w: @options[:width],
              h: @options[:padding_top],
              r: 0, g: 255, b: 0
            }.solid!,
            {
              x: @options[:pos_x],
              y: @options[:pos_y],
              w: @options[:width],
              h: @options[:padding_bottom],
              r: 0, g: 255, b: 0
            }.solid!
          ]
        else
          []
        end
      panel_hash = [
        {
          x: @options[:pos_x],
          y: @options[:pos_y],
          w: @options[:width],
          h: @options[:height],
          r: @options[:bg_color].r,
          g: @options[:bg_color].g,
          b: @options[:bg_color].b,
          a: @options[:bg_transparency]
        }.solid!
      ]
      panel_hash.concat(padding_hash).concat(border_hash).concat(@elements.map(&:render_hash))
    end

    def render(args)
      args.outputs.primitives << render_hash
    end

  end
end
