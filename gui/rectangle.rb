# frozen_string_literal: true

module Projector
  class Rectangle

    def initialize(pos_x, pos_y, width, height, border = 1, color = { r: 0, g: 0, b: 0 })
      @pos_x = pos_x
      @pos_y = pos_y
      @width = width
      @height = height
      @border = border
      @color = color
    end

    def render_hash
      [
        { # border top
          x: @pos_x,
          y: @pos_y + @height,
          h: @border,
          w: @width + @border,
          r: @color[:r],
          g: @color[:g],
          b: @color[:b]
        }.solid!,
        { # border bottom
          x: @pos_x,
          y: @pos_y,
          h: @border,
          w: @width + @border,
          r: @color[:r],
          g: @color[:g],
          b: @color[:b]
        }.solid!,
        { # border left
          x: @pos_x,
          y: @pos_y,
          h: @height + @border,
          w: @border,
          r: @color[:r],
          g: @color[:g],
          b: @color[:b]
        }.solid!,
        { # border right
          x: @pos_x + @width,
          y: @pos_y,
          h: @height + @border,
          w: @border,
          r: @color[:r],
          g: @color[:g],
          b: @color[:b]
        }.solid!
      ]
    end

    def render(args)
      args.outputs.primitives << render_hash
    end

  end
end