# frozen_string_literal: true

module Projector
  ## Definition of class Button
  # Serves to create a button for menus and windows.
  class Button < WindowElement
    attr_accessor :text, :on_click

    def initialize(text, options = {})
      options[:border] = 'full' if options[:border].nil?
      options[:bg_color] = '#bbbddd'
      super(options)
      @options[:width] = get_string_length(text) + 20 if options[:width].zero?
      @options[:height] = get_string_height(text) if options[:height].zero?
      @text = text
      @options[:color_mod] = 0
      @hover_state = false
      @on_click = -> {}
    end

    def check_hover(args)
      if args.inputs.mouse.inside_rect?(render_hash[0])
        @hover_state = true
        @options[:color_mod] = 25 unless args.state.mouse_held
        args.gtk.set_system_cursor('hand')
        if args.inputs.mouse.down
          @options[:color_mod] = 50
          args.state.mouse_held = true
        elsif args.inputs.mouse.up
          @options[:color_mod] = 25
          @on_click.call(args)
          args.state.mouse_held = false
        end
        args.state.inside_button = true
      elsif !@hover_state && args.state.inside_button
        @hover_state = false
        @options[:color_mod] = 0
      else
        @hover_state = false
        @options[:color_mod] = 0
        args.state.inside_button = false
      end
    end

    def render_hash
      [
        {
          x: @options[:pos_x], y: @options[:pos_y],
          w: @options[:width], h: @options[:height],
          r: (@options[:bg_color].r - @options[:color_mod]),
          g: (@options[:bg_color].g - @options[:color_mod]),
          b: (@options[:bg_color].b - @options[:color_mod]),
          a: @options[:bg_transparency]
        }.solid!,
        {
          x: @options[:pos_x] + (@options[:width] / 2),
          y: @options[:pos_y] + @options[:height],
          alignment_enum: 1,
          text: @text,
          font: @options[:font],
          size_enum: @options[:text_size],
          r: @options[:color].r,
          g: @options[:color].g,
          b: @options[:color].b
        }.label!
      ].concat(border_hash)
    end

    def render(args)
      args.outputs.primitives << render_hash
    end

  end
end