# frozen_string_literal: true

module Projector
  class Color
    def self.hex_to_rgb(value)
      color = { r: 0, g: 0, b: 0 }
      value = value.delete('#')
      if valid_hexadecimal?(value)
        values = [0, 0, 0]
        if value.length == 6
          values = value.chars.each_slice(2).map(&:join)
        elsif value.length == 3
          values = (value + value).chars.each_slice(2).map(&:join)
        end
        color = { r: values[0].to_i(16), g: values[1].to_i(16), b: values[2].to_i(16) }
      end
      color
    end

    def self.valid_hexadecimal?(string)
      Integer(string, 16)
      true
    rescue ArgumentError
      false
    end

  end
end